/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Sreeprasad
 */
public class Sales implements Comparable<Object>{
    private static int count = 0;
    private int salesId;
    private String salesPersonName;
    private ArrayList<Order> order;
    private int salesVolume;

    public int getCommission() {
        return commission;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }
    private int commission;
    

    public int getSalesVolume() {
        return salesVolume;
    }

    public void setSalesVolume(int salesVolume) {
        this.salesVolume = salesVolume;
    }

    
    public Sales(){
        count++;
        this.salesId= count;
        order = new ArrayList();
        
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Sales.count = count;
    }

    public int getSalesPersonId() {
        return salesId;
    }

    public void setStorePersonId(int salesId) {
        this.salesId = salesId;
    }

    public ArrayList<Order> getOrderCatalog() {
        return order;
    }

    public void setOrder(ArrayList<Order> order) {
        this.order = order;
    }

    public String getSalesPersonName() {
        return salesPersonName;
    }

    public void setSalesPersonName(String salesPersonName) {
        this.salesPersonName = salesPersonName;
    }

    @Override
    public String toString() {
        return salesPersonName;
    }
    
    
    @Override
    public int compareTo(Object o){
        int compare = ((Sales)o).getSalesVolume();
        return compare-this.salesVolume;
    }
}
