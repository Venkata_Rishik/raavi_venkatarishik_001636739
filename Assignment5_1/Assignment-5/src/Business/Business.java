package Business;

/**
 *
 * @author Sreeprasad
 */
public class Business {

    private SalesDirectory salesDirectory;
    private MasterOrderCatalog masterOrderCatalog;
    private ProductCatalog productCatalog;
    private CustomerDirectory customerDirectory;
    
    public Business() {
        salesDirectory = new SalesDirectory();
        masterOrderCatalog = new MasterOrderCatalog();
        productCatalog = new ProductCatalog();
        customerDirectory = new CustomerDirectory();
    }

    public SalesDirectory getSalesDirectory() {
        return salesDirectory;
    }

    public void setSalesDirectory(SalesDirectory salesDirectory) {
        this.salesDirectory = salesDirectory;
    }

    public ProductCatalog getProductCatalog() {
        return productCatalog;
    }

    public void setProductCatalog(ProductCatalog productCatalog) {
        this.productCatalog = productCatalog;
    }

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    
    public MasterOrderCatalog getMasterOrderCatalog() {
        return masterOrderCatalog;
    }

    public void setMasterOrderCatalog(MasterOrderCatalog masterOrderCatalog) {
        this.masterOrderCatalog = masterOrderCatalog;
    }
    
}
