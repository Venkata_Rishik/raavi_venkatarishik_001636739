/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Sreeprasad
 */
public class CustomerDirectory {
    private ArrayList<Customer> customerList;
    public CustomerDirectory() {
    
        this.customerList = new ArrayList<Customer>();
    }

    public ArrayList<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(ArrayList<Customer> customerList) {
        this.customerList = customerList;
    }
    
    
    public Customer addCustomer(){
        Customer customer = new Customer();
        customerList.add(customer);
        return customer;
    }
    
    public void removeCustomer(Customer customer){
        customerList.remove(customer);
    }
    
    public Customer searchSalesPerson(int keyword){
        for (Customer customer : customerList) {
            if(keyword == customer.getCustomerId()){
                return customer;
            }
        }
        return null;
    } 
}
