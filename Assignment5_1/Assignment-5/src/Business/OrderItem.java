package Business;

/**
 *
 * @author Sreeprasad
 */
public class OrderItem {

    private Product product;
    private int quantity;

    public int getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(int salesPrice) {
        this.salesPrice = salesPrice;
    }
    private int salesPrice;
    
    
        
    
    public Product getProduct() {
        return product;
    }
    
    public void setProduct(Product product) {
        this.product = product;
    }

   
    public int getQuantity() {
        return quantity;
    }
    
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
       
    @Override
    public String toString() {
        return product.getProdName();
    }
    
}
