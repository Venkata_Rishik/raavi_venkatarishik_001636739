/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sreeprasad
 */
public class SalesDirectory {
     private ArrayList<Sales> salesList;
    public SalesDirectory() {
    
        this.salesList = new ArrayList<Sales>();
    }
    
    public ArrayList<Sales> getSaleslist(){
        return salesList;
    }
    
    public Sales addSalesPerson(){
        Sales sales = new Sales();
        salesList.add(sales);
        return sales;
    }
    
    public void removeSalesPerson(Sales sales){
        salesList.remove(sales);
    }
    
    public Sales searchSalesPerson(int keyword){
        for (Sales sales : salesList) {
            if(keyword == sales.getSalesPersonId()){
                return sales;
            }
        }
        return null;
    }
}
