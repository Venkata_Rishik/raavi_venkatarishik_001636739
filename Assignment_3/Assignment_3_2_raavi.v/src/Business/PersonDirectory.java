/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author RishikRaavi
 */
public class PersonDirectory {
    private ArrayList<Person> personDirectory;
   
    public PersonDirectory() {
        personDirectory = new ArrayList<>();
    }

    public ArrayList<Person> getPersonDirectory() {
        return personDirectory;
    }

    public void setPersonDirectory(ArrayList<Person> personDirectory) {
        this.personDirectory = personDirectory;
    }

    
    

    public Person addPerson() {
        Person p = new  Patient ();
        personDirectory.add(p);
        
        return p;
    }

    public void deletePerson(Person p) {
       personDirectory.remove(p);
    }
   
    public Person searchPerson(String personName){
           for(Person p:personDirectory){
               if(p.getPersonName().equals(personName)){
                   return p;
               }
           }
           return null;
       }
}
