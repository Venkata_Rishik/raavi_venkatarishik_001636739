/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author RishikRaavi
 */
public class Person {
    private String personName;
    private String personSsn;
    private int personPhoneNumber;
    private int age;
    private String personGender;

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonSsn() {
        return personSsn;
    }

    @Override
    public String toString() {
        return  personName ;
    }
    

    public void setPersonSsn(String personSsn) {
        this.personSsn = personSsn;
    }

    public int getPersonPhoneNumber() {
        return personPhoneNumber;
    }

    public void setPersonPhoneNumber(int personPhoneNumber) {
        this.personPhoneNumber = personPhoneNumber;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPersonGender() {
        return personGender;
    }

    public void setPersonGender(String personGender) {
        this.personGender = personGender;
    }
    
    
}
