/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic;

import java.util.ArrayList;
import java.util.function.Supplier;

/**
 *
 * @author RishikRaavi
 * 
 */
public class SupplierDirectory {
    
    private String retailerName;
    private String retaileraddress;
    private ArrayList<Suppliers> supplierList;
    
    public SupplierDirectory(){
        supplierList= new ArrayList<>();
    }

    
    public void setRetailerName(String retailerName) {
        this.retailerName = retailerName;
    }
    
    public String getRetailerName() {
        return retailerName;
    }

    public String getRetaileraddress() {
        return retaileraddress;
    }

    public void setRetaileraddress(String retaileraddress) {
        this.retaileraddress = retaileraddress;
    }

    public ArrayList<Suppliers> getSupplierList() {
        return supplierList;
    }

    public void setSupplierList(ArrayList<Suppliers> supplierList) {
        this.supplierList = supplierList;
    }

    public Suppliers addSupplier(){
        Suppliers supplier = new Suppliers();
        supplierList.add(supplier);
        return supplier;
    }
}
