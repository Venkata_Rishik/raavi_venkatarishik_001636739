/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainLogic;

import BusinessLogic.Product;
import BusinessLogic.SupplierDirectory;
import BusinessLogic.Suppliers;

/**
 *
 * @author RishikRaavi
 */
public class MainClass {
  
    public static void main(String args[]){

     SupplierDirectory supplierDirectory = new SupplierDirectory();
     
     supplierDirectory.setRetailerName("Walmart");
     supplierDirectory.setRetaileraddress("America");

     Suppliers s1 = supplierDirectory.addSupplier();
     s1.setSupplierName("Dell");
     s1.setSupplierAddress("Toronto");
     
     Suppliers s2 = supplierDirectory.addSupplier();
     s2.setSupplierName("Lenovo");
     s2.setSupplierAddress("San Jose");

     Suppliers s3 = supplierDirectory.addSupplier();
     s3.setSupplierName("Apple");
     s3.setSupplierAddress("London");
     
     Suppliers s4 = supplierDirectory.addSupplier();
     s4.setSupplierName("Toshiba");
     s4.setSupplierAddress("Indonesia");
     
     Suppliers s5 = supplierDirectory.addSupplier();
     s5.setSupplierName("HP");
     s5.setSupplierAddress("Kolalampur");
     
     Product p11 = s1.getProductCatalog().addProduct();
     p11.setProductName("Dell Laptop 15 inches");
     p11.setProductPrice(Integer.parseInt("1000"));
     p11.setProductQuantity(Integer.parseInt("10"));
     
     Product p12 = s1.getProductCatalog().addProduct();
     p12.setProductName("Dell Laptop 13 inches");
     p12.setProductPrice(Integer.parseInt("900"));
     p12.setProductQuantity(Integer.parseInt("9"));
     
     Product p13 = s1.getProductCatalog().addProduct();
     p13.setProductName("Dell Laptop 10 inches");
     p13.setProductPrice(Integer.parseInt("800"));
     p13.setProductQuantity(Integer.parseInt("6"));
     
     Product p14 = s1.getProductCatalog().addProduct();
     p14.setProductName("Dell Tablet");
     p14.setProductPrice(Integer.parseInt("500"));
     p14.setProductQuantity(Integer.parseInt("8"));
     
     Product p15 = s1.getProductCatalog().addProduct();
     p15.setProductName("Dell Mouse");
     p15.setProductPrice(Integer.parseInt("50"));
     p15.setProductQuantity(Integer.parseInt("9"));
     
     Product p16 = s1.getProductCatalog().addProduct();
     p16.setProductName("Dell Trackpad");
     p16.setProductPrice(Integer.parseInt("10"));
     p16.setProductQuantity(Integer.parseInt("39"));
     
     Product p17 = s1.getProductCatalog().addProduct();
     p17.setProductName("Dell Desktop");
     p17.setProductPrice(Integer.parseInt("200"));
     p17.setProductQuantity(Integer.parseInt("10"));
     
     Product p18 = s1.getProductCatalog().addProduct();
     p18.setProductName("Dell Backpack");
     p18.setProductPrice(Integer.parseInt("100"));
     p18.setProductQuantity(Integer.parseInt("90"));
     
     Product p19 = s1.getProductCatalog().addProduct();
     p19.setProductName("Dell Keyboard");
     p19.setProductPrice(Integer.parseInt("70"));
     p19.setProductQuantity(Integer.parseInt("90"));
     
     Product p10 = s1.getProductCatalog().addProduct();
     p10.setProductName("Dell CPU");
     p10.setProductPrice(Integer.parseInt("150"));
     p10.setProductQuantity(Integer.parseInt("29"));
     
     Product p21 = s2.getProductCatalog().addProduct();
     p21.setProductName("Lenovo Laptop 15 inches");
     p21.setProductPrice(Integer.parseInt("1000"));
     p21.setProductQuantity(Integer.parseInt("10"));
     
     Product p22 = s2.getProductCatalog().addProduct();
     p22.setProductName("Lenovo Laptop 13 inches");
     p22.setProductPrice(Integer.parseInt("900"));
     p22.setProductQuantity(Integer.parseInt("9"));
     
     Product p23 = s2.getProductCatalog().addProduct();
     p23.setProductName("Lenovo Laptop 10 inches");
     p23.setProductPrice(Integer.parseInt("800"));
     p23.setProductQuantity(Integer.parseInt("6"));
     
     Product p24 = s2.getProductCatalog().addProduct();
     p24.setProductName("Lenovo Tablet");
     p24.setProductPrice(Integer.parseInt("500"));
     p24.setProductQuantity(Integer.parseInt("8"));
     
     Product p25 = s2.getProductCatalog().addProduct();
     p25.setProductName("Lenovo Mouse");
     p25.setProductPrice(Integer.parseInt("50"));
     p25.setProductQuantity(Integer.parseInt("9"));
     
     Product p26 = s2.getProductCatalog().addProduct();
     p26.setProductName("Lenovo Trackpad");
     p26.setProductPrice(Integer.parseInt("10"));
     p26.setProductQuantity(Integer.parseInt("39"));
     
     Product p27 = s2.getProductCatalog().addProduct();
     p27.setProductName("Lenovo Desktop");
     p27.setProductPrice(Integer.parseInt("200"));
     p27.setProductQuantity(Integer.parseInt("10"));
     
     Product p28 = s2.getProductCatalog().addProduct();
     p28.setProductName("Lenovo Backpack");
     p28.setProductPrice(Integer.parseInt("180"));
     p28.setProductQuantity(Integer.parseInt("90"));
     
     Product p29 = s2.getProductCatalog().addProduct();
     p29.setProductName("Lenovo Keyboard");
     p29.setProductPrice(Integer.parseInt("70"));
     p29.setProductQuantity(Integer.parseInt("90"));
     
     Product p20 = s2.getProductCatalog().addProduct();
     p20.setProductName("Lenovo CPU");
     p20.setProductPrice(Integer.parseInt("150"));
     p20.setProductQuantity(Integer.parseInt("29"));
     
     
     Product p31 = s3.getProductCatalog().addProduct();
     p31.setProductName("Apple Laptop 15 inches");
     p31.setProductPrice(Integer.parseInt("1000"));
     p31.setProductQuantity(Integer.parseInt("10"));
     
     Product p32 = s3.getProductCatalog().addProduct();
     p32.setProductName("Apple Laptop 13 inches");
     p32.setProductPrice(Integer.parseInt("900"));
     p32.setProductQuantity(Integer.parseInt("9"));
     
     Product p33 = s3.getProductCatalog().addProduct();
     p33.setProductName("Apple Laptop 10 inches");
     p33.setProductPrice(Integer.parseInt("800"));
     p33.setProductQuantity(Integer.parseInt("6"));
     
     Product p34 = s3.getProductCatalog().addProduct();
     p34.setProductName("Apple Tablet");
     p34.setProductPrice(Integer.parseInt("500"));
     p34.setProductQuantity(Integer.parseInt("8"));
     
     Product p35 = s3.getProductCatalog().addProduct();
     p35.setProductName("Apple Mouse");
     p35.setProductPrice(Integer.parseInt("50"));
     p35.setProductQuantity(Integer.parseInt("9"));
     
     Product p36 = s3.getProductCatalog().addProduct();
     p36.setProductName("Apple Trackpad");
     p36.setProductPrice(Integer.parseInt("10"));
     p36.setProductQuantity(Integer.parseInt("39"));
     
     Product p37 = s3.getProductCatalog().addProduct();
     p37.setProductName("Apple Desktop");
     p37.setProductPrice(Integer.parseInt("200"));
     p37.setProductQuantity(Integer.parseInt("10"));
     
     Product p38 = s3.getProductCatalog().addProduct();
     p38.setProductName("Apple Backpack");
     p38.setProductPrice(Integer.parseInt("100"));
     p38.setProductQuantity(Integer.parseInt("90"));
     
     Product p39 = s3.getProductCatalog().addProduct();
     p39.setProductName("Apple Keyboard");
     p39.setProductPrice(Integer.parseInt("70"));
     p39.setProductQuantity(Integer.parseInt("90"));
     
     Product p30 = s3.getProductCatalog().addProduct();
     p30.setProductName("Apple CPU");
     p30.setProductPrice(Integer.parseInt("150"));
     p30.setProductQuantity(Integer.parseInt("29"));
     
     Product p41 = s4.getProductCatalog().addProduct();
     p41.setProductName("Toshiba Laptop 15 inches");
     p41.setProductPrice(Integer.parseInt("1000"));
     p41.setProductQuantity(Integer.parseInt("10"));
     
     Product p42 = s4.getProductCatalog().addProduct();
     p42.setProductName("Toshiba Laptop 13 inches");
     p42.setProductPrice(Integer.parseInt("900"));
     p42.setProductQuantity(Integer.parseInt("9"));
     
     Product p43 = s4.getProductCatalog().addProduct();
     p43.setProductName("Toshiba Laptop 10 inches");
     p43.setProductPrice(Integer.parseInt("800"));
     p43.setProductQuantity(Integer.parseInt("6"));
     
     Product p44 = s4.getProductCatalog().addProduct();
     p44.setProductName("Toshiba Tablet");
     p44.setProductPrice(Integer.parseInt("500"));
     p44.setProductQuantity(Integer.parseInt("8"));
     
     Product p45 = s4.getProductCatalog().addProduct();
     p45.setProductName("Toshiba Mouse");
     p45.setProductPrice(Integer.parseInt("50"));
     p45.setProductQuantity(Integer.parseInt("9"));
     
     Product p46 = s4.getProductCatalog().addProduct();
     p46.setProductName("Toshiba Trackpad");
     p46.setProductPrice(Integer.parseInt("10"));
     p46.setProductQuantity(Integer.parseInt("39"));
     
     Product p47 = s4.getProductCatalog().addProduct();
     p47.setProductName("Toshiba Desktop");
     p47.setProductPrice(Integer.parseInt("200"));
     p47.setProductQuantity(Integer.parseInt("10"));
     
     Product p48 = s4.getProductCatalog().addProduct();
     p48.setProductName("Toshiba Backpack");
     p48.setProductPrice(Integer.parseInt("100"));
     p48.setProductQuantity(Integer.parseInt("90"));
     
     Product p49 = s4.getProductCatalog().addProduct();
     p49.setProductName("Toshiba Keyboard");
     p49.setProductPrice(Integer.parseInt("70"));
     p49.setProductQuantity(Integer.parseInt("90"));
     
     Product p40 = s4.getProductCatalog().addProduct();
     p40.setProductName("Toshiba CPU");
     p40.setProductPrice(Integer.parseInt("150"));
     p40.setProductQuantity(Integer.parseInt("29"));
     
     Product p51 = s5.getProductCatalog().addProduct();
     p51.setProductName("HP Laptop 15 inches");
     p51.setProductPrice(Integer.parseInt("1000"));
     p51.setProductQuantity(Integer.parseInt("10"));
     
     Product p52 = s5.getProductCatalog().addProduct();
     p52.setProductName("HP Laptop 13 inches");
     p52.setProductPrice(Integer.parseInt("900"));
     p52.setProductQuantity(Integer.parseInt("9"));
     
     Product p53 = s5.getProductCatalog().addProduct();
     p53.setProductName("HP Laptop 10 inches");
     p53.setProductPrice(Integer.parseInt("800"));
     p53.setProductQuantity(Integer.parseInt("6"));
     
     Product p54 = s5.getProductCatalog().addProduct();
     p54.setProductName("HP Tablet");
     p54.setProductPrice(Integer.parseInt("500"));
     p54.setProductQuantity(Integer.parseInt("8"));
     
     Product p55 = s5.getProductCatalog().addProduct();
     p55.setProductName("HP Mouse");
     p55.setProductPrice(Integer.parseInt("50"));
     p55.setProductQuantity(Integer.parseInt("9"));
     
     Product p56 = s5.getProductCatalog().addProduct();
     p56.setProductName("HP Trackpad");
     p56.setProductPrice(Integer.parseInt("10"));
     p56.setProductQuantity(Integer.parseInt("39"));
     
     Product p57 = s5.getProductCatalog().addProduct();
     p57.setProductName("HP Desktop");
     p57.setProductPrice(Integer.parseInt("200"));
     p57.setProductQuantity(Integer.parseInt("10"));
     
     Product p58 = s5.getProductCatalog().addProduct();
     p58.setProductName("HP Backpack");
     p58.setProductPrice(Integer.parseInt("100"));
     p58.setProductQuantity(Integer.parseInt("90"));
     
     Product p59 = s5.getProductCatalog().addProduct();
     p59.setProductName("HP Keyboard");
     p59.setProductPrice(Integer.parseInt("70"));
     p59.setProductQuantity(Integer.parseInt("90"));
     
     Product p50 = s5.getProductCatalog().addProduct();
     p50.setProductName("HP CPU");
     p50.setProductPrice(Integer.parseInt("150"));
     p50.setProductQuantity(Integer.parseInt("29"));
     
     System.out.println(" Retailer  " + supplierDirectory.getRetailerName());
     System.out.println(" Retailer address " + supplierDirectory.getRetaileraddress());
    
     for(Suppliers s : supplierDirectory.getSupplierList()){
      System.out.println("Supplier " + s.getSupplierName()); 
      System.out.println(" Supplier address" + s.getSupplierAddress());
      
     for(Product p : s.getProductCatalog().getProductList()){
         System.out.println(" Product name  " + p.getProductName()); 
         System.out.println(" Product Price  " + p.getProductPrice()); 
         System.out.println(" Product Quantity  " + p.getProductQuantity()); 
        
       }
     }   
    }
}
