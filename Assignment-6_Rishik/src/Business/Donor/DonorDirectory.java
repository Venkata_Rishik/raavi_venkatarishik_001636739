/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Business.Donor;

;
import java.util.ArrayList;

/**
 *
 * @author RishikRaavi
 */
public class DonorDirectory {
    private ArrayList<Donor> donorList;
    
    public DonorDirectory(){
        donorList = new ArrayList<>();
        
    }

    public ArrayList<Donor> getDonorList() {
        return donorList;
    }
   
    
    
    public Donor createDonor(String donorName,int donorAge,String bloodGroup){
        Donor donor = new Donor();
        donor.setDonorName(donorName);
        donor.setDonorAge(donorAge);
        donor.setBloodGroup(bloodGroup);
        donorList.add(donor);
        return donor; 
    }
    public void deleteDonor(Donor donor){
        donorList.remove(donor);
    }
    
    public Donor searchDonor(String name){
        for(Donor d:donorList){
            if(d.getDonorName().equalsIgnoreCase(name)){
                return d;
            }
        }
        return null;
    }
}
    
    
  