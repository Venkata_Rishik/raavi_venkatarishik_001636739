/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Business.Donor;

/**
 *
 * @author RishikRaavi */
public class Donor {
    private String donorName;
    private int donorId;
    private int donorAge;
    private String bloodGroup;
    private static int count=1;
    
    public Donor(){
        donorId = count;
        count++;
    }

    public int getDonorAge() {
        return donorAge;
    }

    public void setDonorAge(int donorAge) {
        this.donorAge = donorAge;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getDonorName() {
        return donorName;
    }

    public void setDonorName(String donorName) {
        this.donorName = donorName;
    }

    public int getDonorId() {
        return donorId;
    }

    public void setDonorId(int donorId) {
        this.donorId = donorId;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Donor.count = count;
    }
    @Override
    public String toString(){
        return donorName;
    }
    
}
