/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Business.Roles;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.UserAccount.UserAccount;
import UserInterface.AdministrativeRole.AdminWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author RishikRaavi
 */
public class AdminRole  extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount ua, Organization o, Enterprise e, EcoSystem es) {
        return new AdminWorkAreaJPanel(userProcessContainer, e); //To change body of generated methods, choose Tools | Templates.
    }
    
}
