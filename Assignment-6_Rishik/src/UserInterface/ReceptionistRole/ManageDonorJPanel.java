/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package UserInterface.ReceptionistRole;

import Business.Donor.Donor;
import Business.Donor.DonorDirectory;
import Business.Organization.OrganizationDirectory;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author RishikRaavi
 */
public class ManageDonorJPanel extends javax.swing.JPanel {
    private JPanel userProcessContainer;
    private OrganizationDirectory organizationDir;
    private DonorDirectory donorDirectory;
    public ManageDonorJPanel(JPanel userProcessContainer,OrganizationDirectory organizationDir,DonorDirectory donorDirectory) {
        initComponents();
        
        this.userProcessContainer = userProcessContainer;
        this.organizationDir = organizationDir;
        this.donorDirectory = donorDirectory;
        
        populateTable();
    }

    public void populateTable(){
      DefaultTableModel model = (DefaultTableModel) tblDonor.getModel();
        model.setRowCount(0);
        
            for (Donor d : donorDirectory.getDonorList()) {
                Object row[] = new Object[4];
                row[0] = d;
                row[1] = d.getDonorId();
                row[2] = d.getDonorAge();
                row[3] = d.getBloodGroup();
                
                ((DefaultTableModel) tblDonor.getModel()).addRow(row);
            }
          
    }
    

    /**
     * Creates new form ManageDonorJPanel
     */
   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCreate = new javax.swing.JButton();
        txtDonorAge = new javax.swing.JTextField();
        txtDonorName = new javax.swing.JTextField();
        btnBack = new javax.swing.JButton();
        txtDonorBloodType = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblDonor = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        btnCreate.setText("Create");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });

        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        txtDonorBloodType.setToolTipText("");

        tblDonor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Donor Name", "Id", "Age", "Blood Group"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblDonor);

        jLabel2.setText("Donor Name");

        jLabel4.setText("Donor Blood Type");

        jLabel3.setText("Donor Age");

        jLabel1.setText("Manage Donor");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnBack)
                .addGap(71, 71, 71)
                .addComponent(btnCreate)
                .addGap(176, 176, 176))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtDonorBloodType, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                    .addComponent(txtDonorAge)
                    .addComponent(txtDonorName))
                .addGap(191, 191, 191))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(221, 221, 221)
                        .addComponent(jLabel1)))
                .addContainerGap(94, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(23, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDonorName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDonorAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDonorBloodType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCreate)
                    .addComponent(btnBack))
                .addGap(57, 57, 57))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed

        String donorName = txtDonorName.getText();
        String donorAge = txtDonorAge.getText();
        String donorBloodType = txtDonorBloodType.getText();

        if(donorName == null || donorName.trim().equals("")){
            JOptionPane.showMessageDialog(null,"Donor name cannot be blank");
            return;
        }

        else if(!isAlpha(donorName)){
            JOptionPane.showMessageDialog(null,"Donor name should contain only alphabets");
            return;
        }
        else if(donorAge == null || donorAge.trim().equals("")){
            JOptionPane.showMessageDialog(null,"Donor Age cannot be blank");
            return;
        }

        else if(!isInteger(donorAge)){
            JOptionPane.showMessageDialog(null,"Donor Age should contain only numbers");
            return;
        }
        else if(Integer.parseInt(donorAge) <= 0){
            JOptionPane.showMessageDialog(null,"Only positive numbers are allowed for age");
            return;
        }
        else if(donorBloodType == null || donorBloodType.trim().equals("")){
            JOptionPane.showMessageDialog(null,"Donor blood type cannot be blank");
            return;
        }

        else if(!isAlpha(donorBloodType)){
            JOptionPane.showMessageDialog(null,"Donor blood type should contain only alphabets");
            return;
        }
        else{
            donorDirectory.createDonor(donorName,Integer.parseInt(donorAge),donorBloodType);

            populateTable();
        }
    }//GEN-LAST:event_btnCreateActionPerformed

    
    public boolean isAlpha(String name) 
    {
    char[] chars = name.toCharArray();

    for (char c : chars) {
        if(Character.isLetter(c)) {
            return true;
        }
    }
     return false;
    }
   
   private boolean isInteger(String s) {
     try { 
          Integer.parseInt(s); 
          return true;
         } 
     catch(Exception e) { 
        return false;
         } 
     }
    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCreate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblDonor;
    private javax.swing.JTextField txtDonorAge;
    private javax.swing.JTextField txtDonorBloodType;
    private javax.swing.JTextField txtDonorName;
    // End of variables declaration//GEN-END:variables
}
