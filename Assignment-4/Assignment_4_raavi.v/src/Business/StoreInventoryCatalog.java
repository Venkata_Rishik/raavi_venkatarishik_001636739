/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author RishikRaavi
 */
public class StoreInventoryCatalog {
    private ArrayList<DrugItem> storeInvCatalog;
    
    public StoreInventoryCatalog(){
        storeInvCatalog = new ArrayList<DrugItem>();
    }

    public ArrayList<DrugItem> getStoreInvCatalog() {
        return storeInvCatalog;
    }

    public void setStoreInvCatalog(ArrayList<DrugItem> storeInvCatalog) {
        this.storeInvCatalog = storeInvCatalog;
    }
    
    public DrugItem addDrugItem(Drug d,int q){
        DrugItem di = new DrugItem();
        di.setDrug(d);
        di.setQuant(q);
        storeInvCatalog.add(di);
        return di;
    }
    
    public void deleteDrug(Drug d){
        storeInvCatalog.remove(d);    
    }
     public DrugItem searchPerson(String drugName){
           for(DrugItem p:storeInvCatalog){
               if(p.getDrug().getDrugName().equals(drugName)){
                   return p;
               }
           }
           return null;
       }
    
}

