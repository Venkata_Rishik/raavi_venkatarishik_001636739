/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author RishikRaavi
 */
public class StoreCatalog {
     private ArrayList<Store> storeCatalog;
    
    public StoreCatalog(){
        this.storeCatalog = new ArrayList<Store>();
    }

    public ArrayList<Store> getStoreCatalog() {
        return storeCatalog;
    }

    public void setStoreCatalog(ArrayList<Store> storeCatalog) {
        this.storeCatalog = storeCatalog;
    }
    
    public Store addStore(){
        Store s = new Store();
        storeCatalog.add(s);
        return s;
    }
    
    public void deleteStore(Store s){
        storeCatalog.remove(s);
    }
}
