/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author RishikRaavi
 */
public class SupplierCatalog {
    private ArrayList<Supplier> supDir;
    
    public SupplierCatalog(){
        supDir = new ArrayList<Supplier>();
    }

    public ArrayList<Supplier> getSupDir() {
        return supDir;
    }

    public void setSupDir(ArrayList<Supplier> supDir) {
        this.supDir = supDir;
    }
    
    public Supplier addSupplier(){
        Supplier supplier = new Supplier();
        supDir.add(supplier);
        return supplier;
    }
    
    public void removeSupplier(Supplier s){
        supDir.remove(s);
    }

}
