/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author RishikRaavi
 */
public class Store {
   private String StoreName;
   private int storeId;
   private String StoreLocation;
   private static int count = 0;
   private StoreInventoryCatalog sic;

    
    public StoreInventoryCatalog getSic() {
        return sic;
    }

    public void setSic(StoreInventoryCatalog sic) {
        this.sic = sic;
    }
    
    public Store(){
        count++;
        storeId = count;
        sic = new StoreInventoryCatalog();
    }


    public String getStoreName() {
        return StoreName;
    }

    public void setStoreName(String StoreName) {
        this.StoreName = StoreName;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int StoreId) {
        this.storeId = StoreId;
    }

    

    public String getStoreLocation() {
        return StoreLocation;
    }

    public void setStoreLocation(String StoreLocation) {
        this.StoreLocation = StoreLocation;
    }
    
    @Override
    public String toString() {
        return StoreName;
    }
    
}
