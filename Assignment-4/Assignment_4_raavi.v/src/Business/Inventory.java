/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author RishikRaavi
 */
public class Inventory {
    private ArrayList<DrugItem> inventoryItems;
    private int serialNum;
    
    public Inventory(){
        inventoryItems = new ArrayList<DrugItem>();
    }

    public ArrayList<DrugItem> getInventoryItems() {
        return inventoryItems;
    }

    public void setInventoryItems(ArrayList<DrugItem> inventoryItems) {
        this.inventoryItems = inventoryItems;
    }
    
    public int getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(int serialNum) {
        this.serialNum = serialNum;
    }
    
    public DrugItem addDrugItem(Drug d,int q, int price){
        DrugItem di = new DrugItem();
        di.setDrug(d);
        di.setQuant(q);
        di.setPrice(price);
        inventoryItems.add(di);
        return di;
    }
    
}
