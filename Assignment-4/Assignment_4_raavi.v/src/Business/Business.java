/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author RishikRaavi
 */
public class Business {
    private SupplierCatalog supplierCatalog;
    
    private StoreInventoryCatalog storeInvCatalog;
    private StoreCatalog storeCatalog;
    private DrugCatalog drugCatalog;
    private Inventory inventory;
    private Store store;
    
     public Business(){
        supplierCatalog = new SupplierCatalog();
       
        storeInvCatalog = new StoreInventoryCatalog();
        storeCatalog = new StoreCatalog();
        drugCatalog = new DrugCatalog();
        inventory = new Inventory();
        store=new Store();
    }

    public SupplierCatalog getSupplierCatalog() {
        return supplierCatalog;
    }

    public void setSupplierCatalog(SupplierCatalog supplierCatalog) {
        this.supplierCatalog = supplierCatalog;
    }

   
    public StoreInventoryCatalog getStoreInvCatalog() {
        return storeInvCatalog;
    }

    public void setStoreInvCatalog(StoreInventoryCatalog storeInvCatalog) {
        this.storeInvCatalog = storeInvCatalog;
    }

    public StoreCatalog getStoreCatalog() {
        return storeCatalog;
    }

    public void setStoreCatalog(StoreCatalog storeCatalog) {
        this.storeCatalog = storeCatalog;
    }

    public DrugCatalog getDrugCatalog() {
        return drugCatalog;
    }

    public void setDrugCatalog(DrugCatalog drugCatalog) {
        this.drugCatalog = drugCatalog;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
     
    
}
