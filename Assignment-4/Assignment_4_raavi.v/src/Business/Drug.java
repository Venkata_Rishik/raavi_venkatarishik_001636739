/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author RishikRaavi
 */
public class Drug {
    private int drugid;
    private String drugName;
    private String drugManufDate;
    private String drugExpDate;
    private String composition;
    private String manufacturerName;
    private int price;
    private int weight;
    private String dosage;
    private String type;
    private int qty;

    public int getDrugid() {
        return drugid;
    }

    public void setDrugid(int drugid) {
        this.drugid = drugid;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getDrugManufDate() {
        return drugManufDate;
    }

    public void setDrugManufDate(String drugManufDate) {
        this.drugManufDate = drugManufDate;
    }

    public String getDrugExpDate() {
        return drugExpDate;
    }

    public void setDrugExpDate(String drugExpDate) {
        this.drugExpDate = drugExpDate;
    }

    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
     public String toString() {
        return String.valueOf(drugid);
    }
    
    
    
}
