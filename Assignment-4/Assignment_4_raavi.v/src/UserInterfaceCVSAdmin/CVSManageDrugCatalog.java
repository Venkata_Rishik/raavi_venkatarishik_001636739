/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterfaceCVSAdmin;

import Business.Business;
import Business.Drug;
import Business.DrugItem;
import Business.Supplier;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author RishikRaavi
 */
public class CVSManageDrugCatalog extends javax.swing.JPanel {

    /**
     * Creates new form CVSManageDrugCatalog
     */
    private JPanel userProcessContainer;
    private Business business;
    public CVSManageDrugCatalog(JPanel userProcessContainer, Business business) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.business = business;
        populateSupplierCombo();
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        quantitySpinner = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        supplierComboBox = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        priceText = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        drugCatalogTable = new javax.swing.JTable();
        addInvButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        InventoryTable = new javax.swing.JTable();

        jLabel3.setText("Quantity");

        supplierComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        supplierComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                supplierComboBoxActionPerformed(evt);
            }
        });

        jLabel4.setText("List Price");

        jLabel1.setText("Supplier");

        priceText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                priceTextActionPerformed(evt);
            }
        });

        drugCatalogTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Drug Id", "Drug Name", "Quantity", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(drugCatalogTable);

        addInvButton.setText("Add to Inventory");
        addInvButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addInvButtonActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("CVS Inventory Catalog");

        InventoryTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Drug Id", "Drug Name", "Total Price", "Quantity"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(InventoryTable);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addGap(39, 39, 39)
                            .addComponent(supplierComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(238, 238, 238))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addComponent(jLabel4)
                                .addGap(18, 18, 18)
                                .addComponent(priceText, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(quantitySpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(addInvButton))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(supplierComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addInvButton)
                    .addComponent(quantitySpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(priceText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void supplierComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_supplierComboBoxActionPerformed
        // TODO add your handling code here:
        populateDrugTable();
    }//GEN-LAST:event_supplierComboBoxActionPerformed

    private void addInvButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addInvButtonActionPerformed
        // TODO add your handling code here:
        int selectedRow = drugCatalogTable.getSelectedRow();
        Drug selectedDrug;
        int listPrice=0;
        if(selectedRow<0){
            JOptionPane.showMessageDialog(null, "select a row", "warning", JOptionPane.WARNING_MESSAGE);
            return;
        }else{
            selectedDrug = (Drug) drugCatalogTable.getValueAt(selectedRow, 0);
        }

        try {
            listPrice = Integer.parseInt(priceText.getText());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Enter valid list price", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        if (listPrice < selectedDrug.getPrice()) {
            JOptionPane.showMessageDialog(this, "List Price should be more than actual price", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }

        int fetchedQty = (Integer)quantitySpinner.getValue();
        if(fetchedQty<=0){
            JOptionPane.showMessageDialog(null, "select atleast 1 quantity", "warning", JOptionPane.WARNING_MESSAGE);
            return;
        }else if(fetchedQty<=selectedDrug.getQty()){
            boolean alreadyPresent = false;
            for(DrugItem drugItem : business.getInventory().getInventoryItems()){
                if(drugItem.getDrug() == selectedDrug){
                    int oldAvail = selectedDrug.getQty();
                    int newAvail = oldAvail - fetchedQty;
                    selectedDrug.setQty(newAvail);
                    drugItem.setQuant(fetchedQty + drugItem.getQuant());
                    alreadyPresent = true;
                    populateInventoryTable();
                    populateDrugTable();
                    break;
                }
            }
            if (!alreadyPresent) {
                int oldAvail = selectedDrug.getQty();
                int newAvail = oldAvail - fetchedQty;
                selectedDrug.setQty(newAvail);
                business.getInventory().addDrugItem(selectedDrug, fetchedQty,listPrice);
                populateInventoryTable();
                populateDrugTable();
            }
        }else {
            JOptionPane.showMessageDialog(this, "Quantity > Availability!!", "Warning", JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_addInvButtonActionPerformed

    private void priceTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_priceTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_priceTextActionPerformed

public void populateSupplierCombo(){
supplierComboBox.removeAllItems();
        for(Supplier s: business.getSupplierCatalog().getSupDir())
            supplierComboBox.addItem(s);
        populateDrugTable();
}
 public void populateDrugTable()
    {
        DefaultTableModel dtm = (DefaultTableModel)drugCatalogTable.getModel();
        Supplier supplier = (Supplier)supplierComboBox.getSelectedItem();
        dtm.setRowCount(0);
        if(supplier!=null)
        {
            for(Drug d : supplier.getDrugCatalog().getDrugCatalog()){
                Object row[] = new Object[4];
                row[0] = d;
                row[1] = d.getDrugName();
                row[2] = d.getQty();
                row[3]=d.getPrice();
                dtm.addRow(row);
            }
        }
    }
 public void populateInventoryTable() {
        int rowCount = InventoryTable.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            ((DefaultTableModel) InventoryTable.getModel()).removeRow(i);
        }

        for (DrugItem di : business.getInventory().getInventoryItems()) {
            Object row[] = new Object[4];
            row[0] = di;
            row[1] = di.getDrug().getDrugName();
            row[2] = di.getPrice()* di.getQuant();
            row[3] = di.getQuant();
            
            ((DefaultTableModel) InventoryTable.getModel()).addRow(row);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable InventoryTable;
    private javax.swing.JButton addInvButton;
    private javax.swing.JTable drugCatalogTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField priceText;
    private javax.swing.JSpinner quantitySpinner;
    private javax.swing.JComboBox supplierComboBox;
    // End of variables declaration//GEN-END:variables
}
