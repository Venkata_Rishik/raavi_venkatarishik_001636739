/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.Patient;
import Business.VitalSign;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author RishikRaavi
 */
public class ViewVitalSign extends javax.swing.JPanel {

    /**
     * Creates new form ViewVitalSign
     */
    // private VitalSignHistory vitalSignHistory;
    private Patient patient;

    public ViewVitalSign() {
        initComponents();
    }

    ViewVitalSign(Patient patient) {
        initComponents();
        this.patient = patient;
        display(patient);
        populateTable();

    }

    public void display(Patient patient) {
        pNameJText.setText(patient.getPatientName());
        pAgeJText.setText(String.valueOf(patient.getPatientAge()));
        pIdJText.setText(String.valueOf(patient.getPatientId()));
        pDoctorNameJText.setText(patient.getPrimaryDoctorName());
        prefPharJText.setText(patient.getPatientPreferredPharmacy());

    }

    public void populateTable() {
        DefaultTableModel dtm = (DefaultTableModel) vitalSignTable.getModel();
        System.out.println(patient.getVsh().getVitalSignHistory().size());
        for (int i = (dtm.getRowCount() - 1); i >= 0; i--) {
            dtm.removeRow(i);
        }
        for (VitalSign vitalSign : patient.getVsh().getVitalSignHistory()) {
            System.out.println("hi");
            Object row[] = new Object[2];
            row[0] = vitalSign;

            if (patient.getPatientAge() >= 1 && patient.getPatientAge() <= 3) {
                if ((vitalSign.getRespiratoryRate() >= 20 && vitalSign.getRespiratoryRate() <= 30)
                        && (vitalSign.getHeartRate() >= 80 && vitalSign.getHeartRate() <= 130)
                        && (vitalSign.getSystolicBloodPressure() >= 80 && vitalSign.getSystolicBloodPressure() <= 110)
                        && (vitalSign.getWeightInPounds() >= 22 && vitalSign.getWeightInPounds() <= 31)) {
                    row[1] = "Normal";
                } else {
                    row[1] = "Abnormal";
                }

            } else if (patient.getPatientAge() >= 4 && patient.getPatientAge() <= 5) {
                if ((vitalSign.getRespiratoryRate() >= 20 && vitalSign.getRespiratoryRate() <= 30)
                        && (vitalSign.getHeartRate() >= 80 && vitalSign.getHeartRate() <= 120)
                        && (vitalSign.getSystolicBloodPressure() >= 80 && vitalSign.getSystolicBloodPressure() <= 110)
                        && (vitalSign.getWeightInPounds() >= 31 && vitalSign.getWeightInPounds() <= 40)) {
                    row[1] = "Normal";
                } else {
                    row[1] = "Abnormal";
                }
            } else if (patient.getPatientAge() >= 6 && patient.getPatientAge() <= 12) {
                if ((vitalSign.getRespiratoryRate() >= 20 && vitalSign.getRespiratoryRate() <= 30)
                        && (vitalSign.getHeartRate() >= 70 && vitalSign.getHeartRate() <= 110)
                        && (vitalSign.getSystolicBloodPressure() >= 80 && vitalSign.getSystolicBloodPressure() <= 120)
                        && (vitalSign.getWeightInPounds() >= 41 && vitalSign.getWeightInPounds() <= 92)) {
                    row[1] = "Normal";
                } else {
                    row[1] = "Abnormal";
                }
            } else {
                if ((vitalSign.getRespiratoryRate() >= 12 && vitalSign.getRespiratoryRate() <= 20)
                        && (vitalSign.getHeartRate() >= 55 && vitalSign.getHeartRate() <= 105)
                        && (vitalSign.getSystolicBloodPressure() >= 110 && vitalSign.getSystolicBloodPressure() <= 120)
                        && (vitalSign.getWeightInPounds() > 110)) {
                    row[1] = "Normal";
                } else {
                    row[1] = "Abnormal";
                }

                
            }
            dtm.addRow(row);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        vitalSignTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        respRateJText = new javax.swing.JTextField();
        heartRateJText = new javax.swing.JTextField();
        sysBloodPressureJText = new javax.swing.JTextField();
        weightJText = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        pNameJText = new javax.swing.JTextField();
        pIdJText = new javax.swing.JTextField();
        pAgeJText = new javax.swing.JTextField();
        pDoctorNameJText = new javax.swing.JTextField();
        prefPharJText = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        deleteVitalSignJButton = new javax.swing.JButton();

        vitalSignTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Time", "Condition"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(vitalSignTable);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel1.setText("View Patients Vital Sign ");

        jButton1.setText("View Details Of Selected Vital Sign");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel5.setText("Weight in Pounds");

        jLabel2.setText("Respiratory Rate");

        jLabel3.setText("Heart Rate");

        jLabel4.setText("Systolic Blood Pressure");

        jLabel10.setText("Primary Doctor Name");

        jLabel12.setText("Preferred Pharmacy");

        jLabel7.setText("Patient Name");

        jLabel8.setText("Patient Id");

        jLabel9.setText("Patient Age");

        deleteVitalSignJButton.setText("Delete Vital Sign");
        deleteVitalSignJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteVitalSignJButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(87, 87, 87)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(71, 71, 71)
                                .addComponent(respRateJText, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(sysBloodPressureJText, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(heartRateJText, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(weightJText, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(77, 77, 77)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel7)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12))
                        .addGap(45, 45, 45)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(pNameJText)
                            .addComponent(pIdJText)
                            .addComponent(pAgeJText)
                            .addComponent(pDoctorNameJText)
                            .addComponent(prefPharJText, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addGap(18, 18, 18)
                                .addComponent(deleteVitalSignJButton)))))
                .addContainerGap(55, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(pNameJText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(pIdJText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(pAgeJText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pDoctorNameJText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(prefPharJText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(deleteVitalSignJButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(respRateJText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(heartRateJText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(sysBloodPressureJText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(weightJText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(50, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        int selectedRow = vitalSignTable.getSelectedRow();
        if (selectedRow >= 0) {
            VitalSign vitalSign = (VitalSign) vitalSignTable.getValueAt(selectedRow, 0);
            respRateJText.setText(String.valueOf(vitalSign.getRespiratoryRate()));
            sysBloodPressureJText.setText(String.valueOf(vitalSign.getSystolicBloodPressure()));
            heartRateJText.setText(String.valueOf(vitalSign.getHeartRate()));
            weightJText.setText(String.valueOf(vitalSign.getWeightInPounds()));
        } else {
            JOptionPane.showMessageDialog(null, "Please Select a row");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void deleteVitalSignJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteVitalSignJButtonActionPerformed
        // TODO add your handling code here:
        int selectedRow = vitalSignTable.getSelectedRow();
        if (selectedRow >= 0) {
            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(null, "Would U like to delete", "Warning", dialogButton);
            if (dialogResult == JOptionPane.YES_OPTION) {
                VitalSign vitalSign = (VitalSign) vitalSignTable.getValueAt(selectedRow, 0);

                patient.getVsh().deleteVitalSign(vitalSign);

                populateTable();
                resetFields();
            } else {
                JOptionPane.showMessageDialog(null, "Please Select a row from the table");
            }

        }
    }//GEN-LAST:event_deleteVitalSignJButtonActionPerformed
    public void resetFields() {
        respRateJText.setText("");
        sysBloodPressureJText.setText("");
        heartRateJText.setText("");
        weightJText.setText("");

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton deleteVitalSignJButton;
    private javax.swing.JTextField heartRateJText;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField pAgeJText;
    private javax.swing.JTextField pDoctorNameJText;
    private javax.swing.JTextField pIdJText;
    private javax.swing.JTextField pNameJText;
    private javax.swing.JTextField prefPharJText;
    private javax.swing.JTextField respRateJText;
    private javax.swing.JTextField sysBloodPressureJText;
    private javax.swing.JTable vitalSignTable;
    private javax.swing.JTextField weightJText;
    // End of variables declaration//GEN-END:variables
}
