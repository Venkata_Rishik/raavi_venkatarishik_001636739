/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author RishikRaavi
 */
public class Patient {
    private String patientName;
    private int patientId;
    private int patientAge;
    private String primaryDoctorName;
    private String patientPreferredPharmacy;
    private VitalSignHistory vsh;
    
    public Patient(){
        this.vsh = new VitalSignHistory();
    }
    
    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public int getPatientAge() {
        return patientAge;
    }

    public void setPatientAge(int patientAge) {
        this.patientAge = patientAge;
    }

    public String getPrimaryDoctorName() {
        return primaryDoctorName;
    }

    public void setPrimaryDoctorName(String primaryDoctorName) {
        this.primaryDoctorName = primaryDoctorName;
    }

   

    public String getPatientPreferredPharmacy() {
        return patientPreferredPharmacy;
    }

    public void setPatientPreferredPharmacy(String patientPreferredPharmacy) {
        this.patientPreferredPharmacy = patientPreferredPharmacy;
    }

    public VitalSignHistory getVsh() {
        return vsh;
    }

    public void setVsh(VitalSignHistory vsh) {
        this.vsh = vsh;
    }
    
}
